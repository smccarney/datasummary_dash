#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import pandas as pd
import numpy as np
import pyarrow as pa
import pyarrow.parquet as pq
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate
import dash_table
import re
import postgrest_api
import cat_num
from merge_csv import merge_csv
import pandas_profiling


# Initialize variables
cat_ratios = []
project_name = "GSE94873"
filenames = ["/home/sara_mccarney/Downloads/GSE94873_experiment_data.csv","/home/sara_mccarney/Downloads/GSE94873_meta_data.csv"]
# slug = "Imvigor210"
# aws_access_key, aws_secret_key, region, csv1_url, csv2_url, project_name = postgrest_api.aws_cred(slug)
# filename_urls = [csv1_url, csv2_url]
# bucket_name = re.split("/", csv1_url)[2]
# filenames = [re.split("/", csv1_url)[-1], re.split("/", csv2_url)[-1]]
#
# # df = pd.read_csv(csv1_url, index_col=0, engine='python')
df_meta = merge_csv([filenames[1]], merge=False)
# #df_genome = merge_csv([csv2_url], transpose=True, merge=False)
# tic = time.time()
# df_chunk = pd.read_csv("imvigor210_demo_data/Processed_Data/IMVigor_logCPM_annotated.csv", index_col=0, usecols=[0,1])
# print(df_chunk)
# #print(df_chunk.dtypes.to_dict())
#df_geno
#print(df_genome.head())
# toc = time.time()
# print(toc-tic)
df = merge_csv([filenames[1], filenames[0]], transpose=[False,True])
# pandas_profiling.ProfileReport(df)
# profile = df.profile_report(correlations={
#     "pearson": False,
#     "spearman": False,
#     "kendall": False,
#     "phi_k": False,
#     "cramers": False,
#     "recoded":False}
# )
print(df.dtypes.where(df.dtypes == "object").dropna())
print(df.head())
column_count = len(df.columns)
row_count = len(df.index)
headers = df.columns


na_per_row = df.isnull().sum(axis=1).tolist()
na_per_column = df.isna().sum().tolist()
na_counter_total = np.sum(na_per_column)

series_row = df.isnull().sum(axis=1)
dict_na_rows_sort = (series_row[series_row != 0].sort_values(ascending = False))
series_col = df.isnull().sum()
dict_na_cols_sort = (series_col[series_col != 0].sort_values(ascending = False))
na_per_column_sort = dict_na_cols_sort.tolist()
na_column_names = dict_na_cols_sort.index.tolist()
na_per_row_sort = dict_na_rows_sort.tolist()
na_row_names = dict_na_rows_sort.index.tolist()


object_columns = df.dtypes.where(df.dtypes == "object").dropna()
cat_columns = object_columns.replace("object", "category").to_dict()
numeric_columns = df.dtypes.where(df.dtypes != "object").dropna()
cat_df = pd.DataFrame(df, columns=cat_columns)
num_df = pd.DataFrame(df, columns=numeric_columns)
num_cats = object_columns.count()
num_numerics = numeric_columns.count()
cat_ratios = cat_num.get_category_ratios(cat_df, num_cats, cat_columns)
index_df = df_meta.reset_index()


# if len(df.columns) > 9:
#     show_columns = 8
#     columns = [{"name": df.columns[i], "id": df.columns[i],
#                 "deletable": True, "selectable": True
#                 } for i in range(show_columns)]
#     update_graph = True
# else:
#     columns = [{"name": df.columns[i], "id": df.columns[i],
#                 "deletable": True, "selectable": True} for i in range(len(df.columns))]
#     update_graph = False

# DASH
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)  # Create an instance of Dash

app.layout = html.Div([
    html.H4(project_name),
    html.Div([
        html.Div([
            dcc.Graph(id='dtype_pie', figure={'data':
                                              [{'labels': ['Numerical', 'Categorical'], 'type': 'pie', 'values': [num_numerics/(num_numerics+num_cats), num_cats/(num_numerics+num_cats)],
                                                }], 'layout': {'title': 'Numerical vs. Categeorical datatypes'}
                                              }),
        ], className="six columns"),
        html.Div([
        dash_table.DataTable(
            id='stats',
            # data = [{'Number of catgeorical data types:' : num_cats,
            #         'Number of numerical data types:' : num_numerics}]
            data=[{0: 'Number of columns', 1: column_count},
                  {0: 'Number of rows', 1: row_count},
                  {0: 'Number of categorical data types', 1: num_cats},
                  {0: 'Number of numerical data types', 1: num_numerics},
                  {0: 'Total number of missing values', 1: na_counter_total},
                  {0: 'Filename1', 1: filenames[0]},
                  {0: 'Filename2', 1: filenames[1]}],
            style_header = {'display': 'none'},
            columns=[{"name": '', "id": '0'},
                     {"name": '', "id": '1'}],
            style_table={
            'maxWidth': '300px'},
            style_cell={
        'height': 'auto',
        'minWidth': '220px', 'maxWidth': '250px',
        'whiteSpace': 'normal',
        'textAlign': 'left'}
        )], className="six columns")
    ], className="row"),
    html.Div([

        dash_table.DataTable(
            id='table',
            data=index_df.to_dict('records'),
            style_table={
            'overflowX': 'scroll',
            'maxHeight': '350px',
            'overflowY': 'scroll'},
            page_action = 'none',
            #row_selectable = 'multi',
            editable=True,
            row_deletable=True,
            sort_action='native',
            column_selectable="single",
            selected_columns = [],
            fixed_rows={ 'headers': True, 'data': 0 },
            #fixed_columns={'headers': True,'data': 1},
            #merge_duplicate_headers = True,
            #virtualization = True,
            #selected_rows = [0,1,2,3],
            columns=[{'id': c, 'name': c,
                      "deletable": True, "selectable": True} for c in index_df.columns],
            style_header={
        #'backgroundColor': 'white',
        'fontWeight': 'bold'
    },
            style_cell={
        'height': 'auto',
        'minWidth': '220px', 'maxWidth': '250px',
        'whiteSpace': 'normal',
        'textAlign': 'left',
    }

        ),
        html.Button(children='Submit changes', id='submit_button', n_clicks=0)
    ]),

    html.Div([
        dcc.Graph(id='na_col_bar', figure={'data':
                                           [{'y': na_per_column_sort, 'type': 'bar', 'x': na_column_names,
                                             }], 'layout': {'title': 'Missing values by column', 'yaxis': {'title': 'Number of NA values'},
                                                            'xaxis': {'title': 'Columns with missing NA values'}
                                                            }
                                           }),
    ]),
    html.Div([
        dcc.Graph(id='na_row_bar', figure={'data':
                                           [{'y': na_per_row_sort, 'type': 'bar', 'x': na_row_names,
                                             }], 'layout': {'title': 'Missing values by row', 'yaxis': {'title': 'Number of missing values'}
                                                            #'xaxis': {'title': 'Rows with missing NA values'}
                                                            }
                                           }),
    ]),
    html.Div(id='intermediate-value', style={'display': 'none'}) #Hidden div for storing data in FE

])


# @app.callback(
#     Output('table', 'style_data_conditional'),
#     [Input('table', 'selected_columns')])
#     #[Input('submit_button', 'n_clicks')],
#     #[State('table', 'selected_columns')])
# def update_styles(selected_columns):
#     # if n_clicks>0:
#     col_colour = [{
#         'if': { 'column_id': i },
#         'background_color': '#D2F3FF'
#     } for i in selected_columns]
#     return col_colour



if __name__ == '__main__':
    app.run_server(debug=True)
