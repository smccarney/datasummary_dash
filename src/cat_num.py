import pandas as pd
import numpy as np

def get_category_ratios(cat_df, num_cats, object_columns):
    cat_ratios = []
    #print("num cats:", num_cats)
    for cat in range(num_cats):  # Loop through columns in cat_df
        column_data = cat_df.iloc[:, cat]  # Access the column data in the column
        # Count the frequencies of the unique category values in the column
        frequencies = pd.Series.value_counts(column_data)
        freq_df = pd.DataFrame(frequencies)  # Create a dataframe of the frequencies
        key_list = list(object_columns.keys())
        column_name = key_list[cat]  # Get the column name to index the dataframe
        cat_ratios.append(freq_df[column_name]/np.sum(freq_df[column_name])
                          )  # Calculate the percentages and append to list
    return cat_ratios
