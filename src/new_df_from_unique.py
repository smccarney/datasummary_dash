import pandas as pd
import time

# Sorts dataframe according to unique values of specified column
# Creates n new dataframes where n is the number of unique values
# Returns these dataframes as a list
#Time consuming!!
def new_df_from_unique(df, unique_column):
    list_unique_dataframes = []
    unique_vals = df.loc[:, unique_column].unique()
    for u_value in unique_vals:
        list_unique_dataframes.append(df.where(df[unique_column] == u_value).dropna())

    return list_unique_dataframes, unique_vals

def split_dataframe(df, cat_column, cat_check):
    dfs = []
    for cat in cat_check:
        dfs.append(df.loc[df[cat_column]==cat])
    return dfs

def split_dataframe_num(df, cat_column, quantile_value):
    dfs = []
    dfs.append(df.loc[df[cat_column] < quantile_value])
    dfs.append(df.loc[df[cat_column] >= quantile_value])
    return dfs

def filter_dataframe(df, cat_column, choices, filter_include=True):
    if filter_include:
        unique_vals = choices
    else:
        unique_vals = df.loc[:, cat_column].unique().tolist()
        for choice in choices:
            try:
                unique_vals.remove(choice)
            except:
                continue
    dff = df[df[cat_column].isin(unique_vals)]
    if choices == "":
        dff = df
    print("unique_vals", unique_vals)
    print("dff", dff)
    return dff

def split_sum_dataframe(df, time_column, cat_column, dividers, groupby='sum', cat=True):
    if str(df[cat_column].dtype) == "category":
        dfs = split_dataframe(df, cat_column, dividers)
    elif str(df[cat_column].dtype) != "category":
        dfs = split_dataframe_num(df, cat_column, dividers)
    dfss = []
    if cat == False:
        dfs = [df]
    for df in dfs:
        if groupby == "sum":
            dfss.append(df.groupby([time_column]).sum().reset_index())
        elif groupby == "mean":
            dfss.append(df.groupby([time_column]).mean().reset_index())
        elif groupby == "max":
            dfss.append(df.groupby([time_column]).max().reset_index())
        elif groupby == "min":
            dfss.append(df.groupby([time_column]).min().reset_index())
            print(df.groupby([time_column]).min().reset_index())
    return dfss


# df = pd.read_parquet("~/sonrai-sara/data/covid_19.parquet")
# dfss = split_sum_dataframe(df, "ObservationDate", "Country/Region", ["Mainland China", "US", "UK"], groupby="mean")
# print(dfss)
#
# tic = time.time()
# dfs = new_df_from_unique(df, 'region')
# toc = time.time()
# #print(toc-tic)
#
# tic = time.time()
# df = split_dataframe(df, 'region', 'Albany')
# toc = time.time()
# print(toc-tic)
