import sys
import re
import csv

# Read in plotly colours csv as a list of colours
colours = []
with open('modules/css_plotly_colors.csv', 'rt') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    rows = list(reader)
    for string in rows[0]:
        string = re.sub(r"\s+", "", string)
        colours.append(string)

# The default plotly colours and names I gave them in dictionary format
default_dict = {'mutedblue': '#1f77b4',
                'safetyorange': '#ff7f0e',
                'asparagusgreen':  '#2ca02c',
                'brickred': '#d62728',
                'mutedpurple': '#9467bd',
                'chestnutbrown': '#8c564b',
                'raspberrypink': '#e377c2',
                'middlegray': '#7f7f7f',
                'thaigreen': '#bcbd22',
                'tealblue': '#17becf'}

# Add the default colour names to the list of csv colours names
colour_keys = [key for key in default_dict] + colours
# Add the default colour keys to the list of csv colours keys
colour_values = [value for value in default_dict.values()] + colours

def get_colours(cat_bool, cat_check, num2_bool, colour_value):
    """
    Define a function to get colours for plotting depending on the number of
    lines being plotted and the number of colours given. If there are more lines
    than colours given, the colours will repeat in a loop. If there are more
    colours than lines given, the first colours from the list will be used.
    """

    # Check how many lines are being plotted
    if cat_bool and num2_bool:
        n_lines = len(cat_check) *2
    elif cat_bool == False and num2_bool:
        n_lines =2
    elif cat_bool and num2_bool == False:
        n_lines = len(cat_check)
    else:
        return [colour_value[0]]

    # Use the first n_line colours, if not enough colours, loop the colours
    if n_lines <= len(colour_value):
        used_colours = colour_value[:n_lines]
    else:
        multiply_colours = n_lines//len(colour_value)
        append_colours = n_lines%len(colour_value)
        used_colours = colour_value * multiply_colours
        used_colours.extend([colour_value[colour] for colour in range(append_colours)])

    return used_colours
