# Data Summary App

This project contains the code required to generate a DASH app that provides
the user with some statistics about their data, for example the number of
missing values and the dimensions of the data, as well as providing a data
preview as an interactive table.
